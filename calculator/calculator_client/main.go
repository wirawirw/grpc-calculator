package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
	"my-grpc/calculator/proto/calculator"
)

func main()  {
	fmt.Println("Dial to server")
	c,err:=grpc.Dial("127.0.0.1:9090",grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error when trying to dial %v",err)
	}

	decomposition(c)
}

func calculate(c *grpc.ClientConn)  {
	service:=calculator.NewCalculatorServiceClient(c)

	res,e:=service.Calculate(context.Background(),&calculator.CalculateRequest{
		Number1: 10,
		Number2: 3,
	})

	if e != nil {
		log.Fatalf("Error when trying to calculate %v",e)
	}else{
		log.Printf("Result: %v",res)
	}
}

func decomposition(c *grpc.ClientConn)  {
	service:=calculator.NewDecompositionServiceClient(c)
	streamDecomposition,err:=service.Decomposition(context.Background(),&calculator.DecompositionRequest{Number: 120})
	if err != nil {
		log.Fatalf("%v",err)
	}

	for  {
		msg,err:=streamDecomposition.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("Failed to get streamer data %v",err)
		}

		fmt.Println(msg.GetResult())
	}
}
