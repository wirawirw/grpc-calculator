package grpc_server

import (
	"context"
	"my-grpc/calculator/proto/calculator"
)

type CalculatorServer struct {}

func (c CalculatorServer) Calculate(ctx context.Context, request *calculator.CalculateRequest) (*calculator.CalculateResponse, error) {
	sum:=request.Number1+request.Number2
	return &calculator.CalculateResponse{Result: sum},nil
}

