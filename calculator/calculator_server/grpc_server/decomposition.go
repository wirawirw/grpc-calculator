package grpc_server

import "my-grpc/calculator/proto/calculator"

type DecompositionServer struct {
	
}

func (d DecompositionServer) Decomposition(request *calculator.DecompositionRequest, server calculator.DecompositionService_DecompositionServer) error {

	var k int64
	k=2
	n:=request.GetNumber()

	for request.Number>1 {
		if  n%k==0 {
			server.Send(&calculator.DecompositionResponse{Result: k})
			n=n/k
		}else{
			k=k+1
		}
	}

	return nil
}

