package main

import (
	"google.golang.org/grpc"
	"log"
	"my-grpc/calculator/calculator_server/grpc_server"
	"my-grpc/calculator/proto/calculator"
	"net"
)

func main()  {
	c,err:=net.Listen("tcp","0.0.0.0:9090")
	if err != nil {
		log.Fatalf("Error when try to listening: %v",err)
	}

	s:=grpc.NewServer()
	calculator.RegisterCalculatorServiceServer(s,grpc_server.CalculatorServer{})
	calculator.RegisterDecompositionServiceServer(s,grpc_server.DecompositionServer{})
	err=s.Serve(c)

	if err != nil {
		log.Fatalf("Error when try to serve: %v",err)
	}
}

