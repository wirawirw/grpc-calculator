package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
	"my-grpc/greet/greetpb"
)

func main()  {
	fmt.Println("Setup client")
	conn,err:=grpc.Dial("localhost:9001",grpc.WithInsecure())
	c:=greetpb.NewGreetServiceClient(conn)

	if err != nil {
		log.Fatalf("could not connect: %v",err)
	}

	//doUnary(c)
	doStreamServer(c)

	defer conn.Close()
}

func doStreamServer(c greetpb.GreetServiceClient) {
	fmt.Println("Do Streaming RPC")

	req:=greetpb.GreetManyTimesRequest{Greeting: &greetpb.Greeting{
		FirstName: "Wiranatha",
		LastName:  "Gde",
	}}

	resStream,err:=c.GreetManyTimes(context.Background(),&req)

	if err != nil {
		log.Fatalf("Failed to load streamer from server: %v",err)
	}
	for  {
		msg,err:=resStream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("Failed to get streamer data %v",err)
		}

		fmt.Println(msg.GetResult())
	}

}

func doUnary(c greetpb.GreetServiceClient)  {
	fmt.Println("Do Unary RPC")

	req:=&greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Wira",
			LastName:  "Natha",
		},
	}
	res,err:=c.Greet(context.Background(),req)
	if err != nil {
		log.Fatalf("Error while calling greet RPC: %v",err)
	}
	log.Printf("Response from Greet: %v",res.Result)
}