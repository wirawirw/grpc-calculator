package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"my-grpc/greet/greetpb"
	"net"
	"time"
)


func main()  {
	fmt.Println("Hello World")
	lis,err:=net.Listen("tcp","0.0.0.0:9001")
	if err != nil {
		log.Fatalf("Failed to listen: %v",err)
	}

	s:=grpc.NewServer()
	greetpb.RegisterGreetServiceServer(s,&server{})

	if err != s.Serve(lis) {
		log.Fatalf("Failed to serve: %v",err)
	}
}


type server struct {

}

func (s *server) GreetManyTimes(request *greetpb.GreetManyTimesRequest, stream greetpb.GreetService_GreetManyTimesServer) error {
	for i := 0; i < 10; i++ {
		err:=stream.Send(&greetpb.GreetManyTimesResponse{
			Result: "Hello  "+request.GetGreeting().GetFirstName()+" "+string(i),
		})

		if err != nil {
			log.Fatalf("Error when send response %v",err)
		}
		time.Sleep(1000*time.Millisecond)
	}

	return nil
}

func (s *server) Greet(ctx context.Context, request *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	firstName:=request.GetGreeting().GetFirstName()
	lastName:=request.GetGreeting().GetLastName()

	result:="Hello "+firstName+" "+lastName
	res:=&greetpb.GreetResponse{
		Result: result,
	}

	return res,nil
}

